package com.company;

class Bike {

    String color;
    String mark;
    String engine;
    int power;
    String ime;
    String prezime;
    String Address;

    public Bike(String colr, String mrk, String engine, int power, String ime, String prezime, String Address) {

        this.color = colr;
        this.mark = mrk;
        this.engine = engine;
        this.power = power;
        this.ime = ime;
        this.prezime = prezime;
        this.Address = Address;
    }

    public String getColor() {
        return color;
    }

    public String getMark() {
        return mark;
    }

    public String getEngine() {
        return engine;
    }

    public String getime() {
        return ime;
    }

    public String getprezime() {
        return prezime;
    }

    public String getAdress() {
        return Address;
    }

    public int getpower() {
        return power;
    }
}

class MotorBike extends Bike {


    private String startMethod = "Motocicle";

    public MotorBike() {
        this("Motocicle");
    }

    public MotorBike(String method) {
        super("Red", "Yamaha MT-125", "V9", 59, "Ivan", "Panks", "Mija ivana7");
        this.startMethod = method;
    }





    public void display() {
        System.out.println("----Motocicle data-----");
        System.out.println("Colour :" + super.getColor());
        System.out.println("Marks:" + super.getMark());
        System.out.println("Engine:" + super.getEngine());
        System.out.println("Power:" + super.getpower());
        System.out.println("------Owners data------");
        System.out.println("Ime:" + super.getime());
        System.out.println("Prezime:" + super.getprezime());
        System.out.println("Adresa:" + super.getAdress());
        System.out.println("Start Method:" + this.startMethod);


    }



}
